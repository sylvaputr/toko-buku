<?php

namespace App\Controllers;

use App\Models\BukuModel;
use App\Models\KategoriModel;

class Buku extends BaseController
{

	protected $buku;

	public function __construct()
	{
		//		parent::__construct();
		$this->buku = new BukuModel();
		$this->kategori = new KategoriModel();

		helper('form');
	}

	public function index()
	{
		$data['buku'] = $this->buku->getBookData();
		return view('buku/index', $data);
	}

	public function add()
	{
		$data['kategori'] = $this->kategori->findAll();
		return view('buku/add', $data);
	}

	public function store()
	{
		if (!$this->validate([
			'category_id' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} Harus diisi'
				]
			],
			'title' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Judul Buku Harus diisi'
				]
			],
			// 'photo' => [
			// 	'rules' => 'required',
			// 	'errors' => [
			// 		'required' => 'Foto Harus diisi'
			// 	]
			// ],
			'description' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Deskripsi Harus diisi'
				]
			],
			'writer' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Penulis Harus diisi'
				]
			],
			'publisher' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Penerbit Harus diisi'
				]
			],
			'total_page' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Total Halaman Harus diisi'
				]
			],
			'release_date' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Tangal RilisHarus diisi'
				]
			],
			'isbn' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'ISBN Harus diisi'
				]
			],
			'language' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Bahasa Harus diisi'
				]
			],
			'weight' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Berat Harus diisi'
				]
			],
			'width' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Lebar Harus diisi'
				]
			],
			'height' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Panjang Harus diisi'
				]
			],
			'price' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Harga Harus diisi'
				]
			],
			'stock' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Stok Harus diisi'
				]
			],

		])) {
			session()->setFlashdata('error', $this->validator->listErrors());
			return redirect()->back()->withInput();
		}
		$photoFile = $this->request->getFile('photo');
		$fileName = $photoFile->getRandomName();
		$photoFile->move('uploads/photos/', $fileName);
		$this->buku->insert([
			'category_id' => $this->request->getVar('category_id'),
			'title' => $this->request->getVar('title'),
			'description' => $this->request->getVar('description'),
			'photo' => $fileName,
			'writer' => $this->request->getVar('writer'),
			'publisher' => $this->request->getVar('publisher'),
			'total_page' => $this->request->getVar('total_page'),
			'release_date' => $this->request->getVar('release_date'),
			'isbn' => $this->request->getVar('isbn'),
			'language' => $this->request->getVar('language'),
			'weight' => $this->request->getVar('weight'),
			'width' => $this->request->getVar('width'),
			'height' => $this->request->getVar('height'),
			'price' => $this->request->getVar('price'),
			'stock' => $this->request->getVar('stock')
		]);
		session()->setFlashdata('message', 'Tambah Buku Berhasil');
		return redirect()->to('/Buku');
	}

	function edit($id)
	{
		$dataBuku = $this->buku->find($id);
		if (empty($dataBuku)) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Buku Tidak ditemukan !');
		}
		$data['buku'] = $dataBuku;
		$data['kategori'] = $this->kategori->findAll();
		return view('Buku/edit', $data);
	}

	public function update($id)
	{
		if (!$this->validate([
			'category_id' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} Harus diisi'
				]
			],
			'title' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Judul Buku Harus diisi'
				]
			],
			'photo' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Foto Harus diisi'
				]
			],
			'description' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Deskripsi Harus diisi'
				]
			],
			'writer' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Penulis Harus diisi'
				]
			],
			'publisher' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Penerbit Harus diisi'
				]
			],
			'total_page' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Total Halaman Harus diisi'
				]
			],
			'release_date' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Tangal RilisHarus diisi'
				]
			],
			'isbn' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'ISBN Harus diisi'
				]
			],
			'language' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Bahasa Harus diisi'
				]
			],
			'weight' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Berat Harus diisi'
				]
			],
			'width' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Lebar Harus diisi'
				]
			],
			'height' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Panjang Harus diisi'
				]
			],
			'price' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Harga Harus diisi'
				]
			],
			'stock' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Stok Harus diisi'
				]
			],

		])) {
			session()->setFlashdata('error', $this->validator->listErrors());
			return redirect()->back();
		}

		$this->buku->update($id, [
			'category_id' => $this->request->getVar('category_id'),
			'title' => $this->request->getVar('title'),
			'photo' => $this->request->getVar('photo'),
			'description' => $this->request->getVar('description'),
			'writer' => $this->request->getVar('writer'),
			'publisher' => $this->request->getVar('publisher'),
			'total_page' => $this->request->getVar('total_page'),
			'release_date' => $this->request->getVar('release_date'),
			'isbn' => $this->request->getVar('isbn'),
			'language' => $this->request->getVar('language'),
			'weight' => $this->request->getVar('weight'),
			'width' => $this->request->getVar('width'),
			'height' => $this->request->getVar('height'),
			'price' => $this->request->getVar('price'),
			'stock' => $this->request->getVar('stock')
		]);

		session()->setFlashdata('message', 'Update Data Buku Berhasil');
		return redirect()->to('/Buku');
	}

	function delete($id)
	{
		$dataBuku = $this->buku->find($id);
		if (empty($dataBuku)) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Buku Tidak ditemukan !');
		}
		$this->buku->delete($id);
		session()->setFlashdata('message', 'Delete Data Buku Berhasil');
		return redirect()->to('/Buku');
	}
}
