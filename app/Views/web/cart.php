<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>BooKU</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/css/mod.css" rel="stylesheet">
</head>

<body>
    <!-- Topbar Start -->
    <div class="top-bar-booku">
        <div class="container-fluid">
            <div class="row align-items-center bg-light py-3 px-xl-5 d-none d-lg-flex">
                <div class="col-lg-4">
                    <a href="" class="text-decoration-none">
                        <span class="h1 text-uppercase text-white bg-custom px-2">Boo</span>
                        <span class="h1 text-uppercase text-white bg-ku px-2 ml-n1">KU</span>
                    </a>
                </div>
                <div class="col-lg-5 col-6 text-left">
                    <nav class="navbar navbar-expand-lg bg-white navbar-dark py-3 py-lg-0 px-0">
                        <a href="" class="text-decoration-none d-block d-lg-none">
                            <span class="h1 text-uppercase text-dark bg-light px-2">Multi</span>
                            <span class="h1 text-uppercase text-light bg-primary px-2 ml-n1">Shop</span>
                        </a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto py-0">
                                <a href="<?= base_url(); ?>/Web" class=" nav-item nav-link">Beranda</a>
                                <a href="shop.html" class="nav-item nav-link active">Produk</a>
                                <a href="detail.html" class="nav-item nav-link">Blog</a>
                                <a href="contact.html" class="nav-item nav-link">Tentang Kami</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->

    <!-- Cart Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-8 table-responsive mb-5">
                <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th>Judul</th>
                            <th>Penerbit</th>
                            <th>Harga</th>
                            <th>Kuantitas</th>
                            <th>Total</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle">
                        <?php 
                            $no = 1;
                        foreach ($cart as $row) {
                            $no++;
                        ?>
                        <form action="<?= base_url(); ?>/Keranjang/update" method="post" class="form">
                        <tr>
                            <td class="align-middle"><?= $row['name']?></td>
                            <td class="align-middle"><?= $row['options']['publisher']?></td>
                            <td class="align-middle">Rp<?= number_format($row['price'], 0, '.', ',');?></td>
                            <td class="align-middle">
                                <button class="btn-quantity" onclick="minQty(<?= $row['qty']; ?>,<?= $no?>)" type="submit"><i class="fa fa-minus" ></i></button>
                                <input type="number" id="qty<?=$no ?>" name="qty" value="<?= $row['qty']?>" class="col-lg-5" style="text-align:center;" readonly="true"></input>
                                <input type="hidden" name="id" value="<?= $row['id']?>"></input>
                                <input type="hidden" name="rowid" value="<?= $row['rowid']?>"></input>
                                <input type="hidden" name="price" value="<?= $row['price']?>"></input>
                                <input type="hidden" name="name" value="<?= $row['name']?>"></input>
                                <input type="hidden" name="publisher" value="<?= $row['options']['publisher']?>"></input>
                                <button class="btn-quantity" onclick="addQty(<?= $row['qty']; ?>, <?= $no?>)" type="submit"><i class="fa fa-plus"></i></button>
                            </td>
                            <td class="align-middle">Rp<?= number_format($row['subtotal'], 0, '.', ',')?></td>
                        </form>
                            <form action="<?= base_url(); ?>/Keranjang/remove" method="post" class="form">
                                <input type="hidden" name="rowid" value="<?= $row['rowid']?>"></input>
                                <td class="align-middle"><button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>
                            </form>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4">
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="pr-3">Rincian Belanja</span></h5>
                <div class="bg-light p-30 mb-5">
                    <div class="pt-2">
                        <div class="d-flex justify-content-between mt-2">
                            <h5>Total Belanja</h5>
                            <h5>
                                <?php 
                                $totalPrice = 0;
                                foreach ($cart as $row) {     
                                    $totalPrice = $totalPrice + $row['subtotal']; 
                                }
                                 echo 'Rp'.number_format($totalPrice, 0, '.', ',');
                                ?>
                                
                            </h5>
                        </div>
                        <button class="btn btn-block font-weight-bold my-3" data-toggle="modal" data-target="#exampleModal" style="background-color: #f76e11;color: white;">Checkout</button>
                        <!-- Modal Dialog -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Masukkan nama pemesan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="<?= base_url(); ?>/Keranjang/checkout" method="post" class="form">
                            <div class="modal-body">
                                <div class="form-group">
                                <input type="text" id="buyer" class="form-control" placeholder="Masukkan nama" name="buyer_name">
                                <input type="hidden" name="total_price" value="<?=$totalPrice;?>"></input>
                                <input type="hidden" name="cart_data" value="<?php echo htmlspecialchars(json_encode($cart)); ?>"></input>
                            </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn" style="background-color: #f76e11;color: white;">Pesan Sekarang</button>
                            </div>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->


    <!-- Footer Start -->
    <div class="footer container-fluid text-dark">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Tentang BooKU</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Informasi</a>
                            <a class="text-dark mb-2" href="#">Toko Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4 bold">Bantuan</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">FAQ</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Pengembalian</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Privasi</a>
                            <a class="text-dark mb-2" href="#">Aksesibilitas</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Membership</a>
                            <a class="text-dark mb-2" href="#">Profil</a>
                            <a class="text-dark mb-2" href="#">Kupon</a>
                            <a class="text-dark mb-2" href="#">Kontak Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun Sosial BooKU</p>
                        <div class="d-flex btn-footer">
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-square" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border-top py-3" style="background: #F76E11;">
            <div class="col-md-12 px-xl-0">
                <h6 class="mb-md-0 text-center text-white">
                    © 2022. PT Kelompok 1
                </h6>
            </div>
        </div>
    </div>
    <!-- Footer End -->

    <script>
        // window.onload = function(){
        //     // document.getElementById("qty").innerHTML = $qty;
        // };

        function addQty(qty, no) {
            document.getElementById(`qty${no}`).value = ++qty;
        }

        function minQty(qty, no) {
           if (qty > 1) {
            document.getElementById(`qty${no}`).value = --qty;
           }
        }
    </script>


    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/easing/easing.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?= base_url(); ?>/web-assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?= base_url(); ?>/web-assets/js/main.js"></script>
</body>

</html>