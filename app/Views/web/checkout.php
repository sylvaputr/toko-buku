<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>BooKU</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/css/mod.css" rel="stylesheet">
</head>

<body>
    <!-- Topbar Start -->
    <div class="top-bar-booku">
        <div class="container-fluid">
            <div class="row align-items-center bg-light py-3 px-xl-5 d-none d-lg-flex">
                <div class="col-lg-4">
                    <a href="" class="text-decoration-none">
                        <span class="h1 text-uppercase text-white bg-custom px-2">Boo</span>
                        <span class="h1 text-uppercase text-white bg-ku px-2 ml-n1">KU</span>
                    </a>
                </div>
                <div class="col-lg-5 col-6 text-left">
                    <nav class="navbar navbar-expand-lg bg-white navbar-dark py-3 py-lg-0 px-0">
                        <a href="" class="text-decoration-none d-block d-lg-none">
                            <span class="h1 text-uppercase text-dark bg-light px-2">Multi</span>
                            <span class="h1 text-uppercase text-light bg-primary px-2 ml-n1">Shop</span>
                        </a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto py-0">
                                <a href="<?= base_url(); ?>/Web" class=" nav-item nav-link">Beranda</a>
                                <a href="shop.html" class="nav-item nav-link active">Produk</a>
                                <a href="detail.html" class="nav-item nav-link">Blog</a>
                                <a href="contact.html" class="nav-item nav-link">Tentang Kami</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->
    <?php if (!empty(session()->getFlashdata('message'))) : ?>
        <div class="alert alert-success alert-dismissible fade show mr-2 ml-2 mt-0" role="alert">
            <?php echo session()->getFlashdata('message'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <!-- Cart Start -->
    <div class="checkout">
        <div class="container-fluid">
            <div class="row px-xl-5">
                <div class="col-lg-8 table-responsive mb-5">
                    <h5 class="section-title position-relative text-uppercase mb-3"><span class="pr-3">Tas
                            Belanja</span></h5>
                    <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th>Judul</th>
                            <th>Penerbit</th>
                            <th>Harga</th>
                            <th>Kuantitas</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle">
                        <?php 
                            $no = 1;
                        foreach ($cart as $row) {
                            $no++;
                        ?>
                        <form action="<?= base_url(); ?>/Keranjang/update" method="post" class="form">
                        <tr>
                            <td class="align-middle"><?= $row['name']?></td>
                            <td class="align-middle"><?= $row['options']['publisher']?></td>
                            <td class="align-middle">Rp<?= number_format($row['price'], 0, '.', ',');?></td>
                            <td class="align-middle">
                                <label><?= $row['qty']?></label>
                            </td>
                            <td class="align-middle">Rp<?= number_format($row['subtotal'], 0, '.', ',')?></td>
                        </form>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                </div>
                <div class="col-lg-4">
                    <div class="card rincian-belanja">
                        <div class="card-body pb-0">
                            <h5 class="section-title position-relative text-uppercase mb-3"><span class="pr-3">Rincian
                                    Belanja</span></h5>
                            <hr>
                            <div class="pt-2">
                                <div class="d-flex justify-content-between mt-2">
                                    <h6>Kode Pesanan</h6>
                                    <h6><strong><?=$order_code?></strong></h6>
                                </div>
                                <div class="d-flex justify-content-between mt-2">
                                    <h6>Nama Pemesan</h6>
                                    <h6><?= $buyer_name ?></h6>
                                </div>
                                <div class="d-flex justify-content-between mt-2 mb-3">
                                    <h6>Total Belanja</h6>
                                    <h6><strong><?php 
                                $totalPrice = 0;
                                foreach ($cart as $row) {     
                                    $totalPrice = $totalPrice + $row['subtotal']; 
                                }
                                 echo 'Rp'.number_format($totalPrice, 0, '.', ',');
                                ?></strong></h6>
                                </div>
                                <p>*Beritahu kasir kode pesanan atau nama pemesan saat melakukan
                                    pembayaran
                                </p>
                                <a class="btn btn-block font-weight-bold my-3" style="background-color: #f76e11;color: white;"
                                    href="<?= base_url(); ?>/Web" >Selesai</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->


    <!-- Footer Start -->
    <div class="footer container-fluid text-dark">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Tentang BooKU</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Informasi</a>
                            <a class="text-dark mb-2" href="#">Toko Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4 bold">Bantuan</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">FAQ</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Pengembalian</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Privasi</a>
                            <a class="text-dark mb-2" href="#">Aksesibilitas</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Membership</a>
                            <a class="text-dark mb-2" href="#">Profil</a>
                            <a class="text-dark mb-2" href="#">Kupon</a>
                            <a class="text-dark mb-2" href="#">Kontak Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun Sosial BooKU</p>
                        <div class="d-flex btn-footer">
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-square" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border-top py-3" style="background: #F76E11;">
            <div class="col-md-12 px-xl-0">
                <h6 class="mb-md-0 text-center text-white">
                    © 2022. PT Kelompok 1
                </h6>
            </div>
        </div>
    </div>
    <!-- Footer End -->

    <script>
        // window.onload = function(){
        //     // document.getElementById("qty").innerHTML = $qty;
        // };

        function addQty(qty, no) {
            document.getElementById(`qty${no}`).value = ++qty;
        }

        function minQty(qty, no) {
           if (qty > 1) {
            document.getElementById(`qty${no}`).value = --qty;
           }
        }
    </script>


    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/easing/easing.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?= base_url(); ?>/web-assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?= base_url(); ?>/web-assets/js/main.js"></script>
</body>

</html>