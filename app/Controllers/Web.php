<?php

namespace App\Controllers;

use App\Models\BukuModel;
use App\Models\KategoriModel;

class Web extends BaseController
{
	public function __construct()
	{
		//		parent::__construct();
		$this->db = \Config\Database::connect();
		$this->email = \Config\Services::email();
		$this->buku = new BukuModel();
		$this->kategori = new KategoriModel();
		$this->cart = \Config\Services::cart();

		helper('form');
	}

	public function index()
	{
		$data['buku'] = $this->buku->getBookData();
		$data["cart"] = $this->cart->totalItems();
		return view('web/index', $data);
	}

	public function detail($id)
	{
		$data['buku'] = $this->buku->find($id);
		$data["cart"] = $this->cart->totalItems();
		return view('web/detail', $data);
	}
}
