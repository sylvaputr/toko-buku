<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderDetailModel extends Model
{
    protected $table      = 'detail_order';
    protected $primaryKey = 'order_id';
    protected $foreignKey = 'book_id';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['order_id', 'book_id', 'title', 'publisher', 'total_item', 'price', 'subtotal'];
}
