<?php

namespace App\Models;

use CodeIgniter\Model;

class BukuModel extends Model
{
    protected $table      = 'book';
    protected $primaryKey = 'book_id';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['category_id', 'title', 'photo', 'description', 'writer', 'publisher', 'total_page', 'release_date', 'isbn', 'language', 'weight', 'width', 'height', 'price', 'stock'];

    public function getBookData()
    {
         return $this->db->table('book')
         ->join('category','category.category_id=book.category_id')
         ->get()->getResultArray();  
    }
}
