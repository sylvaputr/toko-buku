<?php

namespace App\Controllers;

use App\Models\OrderModel;
use App\Models\OrderDetailModel;

class Order extends BaseController
{

	protected $order;

	public function __construct()
	{
		//		parent::__construct();
		$this->order = new OrderModel();
        $this->orderDetail = new OrderDetailModel();

		helper('form');
	}

	public function index()
	{
		$data['order'] = $this->order->findAll();
		return view('order/index', $data);
	}

    public function detail() {
         if($this->request->isAJAX()){
            // Select record
            $query = service('request')->getPost('order_id');
            $response  = $this->orderDetail->where('order_id', $query)->findAll(); 
            echo json_encode($response);
     }
    }

    public function update($id)
	{
		$this->order->update($id, [
			'status' => 1
		]);
		session()->setFlashdata('message', 'Pesanan sudah dibayar');
		return redirect()->to('/Order');
	}

    public function report() {
        $qty= $this->order->sumQuantities();
        $revenue = $this->order->sumRevenue();
		$data['totalQty'] = $qty[0]['total_item'];
		$data['totalRevenue'] = $revenue[0]['total_price'];
        $data['order'] = $this->order->where('status', 1)->findAll();

        // dd($data['order']);
		return view('order_report/index', $data);
    }

}
