<?php
namespace App\Controllers;
use App\Models\OrderModel;
use App\Models\OrderDetailModel;

class Keranjang extends BaseController
{
    public function __construct()
	{
		//		parent::__construct();
		$this->order = new OrderModel();
		$this->orderDetail = new OrderDetailModel();
        $this->cart = \Config\Services::cart();
        helper('number');
		helper('form');
	}

    public function index()
    {
        $data["cart"] = $this->cart->contents();
        return view('web/cart', $data); //passing data
    }

    public function check() {
        $response = $this->cart->contents();
        $data = json_encode($response);
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    //method tambah produk ke keranjang
    public function add()
    {
        // dd($this->request->getVar());
        $this->cart->insert(array(
            'id'      => $this->request->getVar('id'),
            'qty'     => $this->request->getVar('qty'),
            'price'   => $this->request->getVar('price'),
            'name'    => $this->request->getVar('name'),
            'options' => array('publisher' => $this->request->getVar('publisher'))
        ));
        session()->setFlashdata('message', 'Buku berhasil ditambahkan ke keranjang');
        return redirect()->to(base_url('Web/detail/'.$this->request->getVar('id')));
    }

    public function update() {
        $this->cart->update(array(
            'rowid'   => $this->request->getVar('rowid'),
            'id'      => $this->request->getVar('id'),
            'qty'     => $this->request->getVar('qty'),
            'price'   => $this->request->getVar('price'),
            'name'    => $this->request->getVar('name'),
            'options' => array('publisher' => $this->request->getVar('publisher'))
        ));
        return redirect()->to(base_url('keranjang/'));
    }

    //method load data
    public function load()
    {
        echo $this->view();
    }

    //method hapus produk dari keranjang
    public function remove()
    {
        // ambil id unique dari product mau di hapus dari cart, bukan id dari database
        $row_id = $this->request->getVar('rowid');
        $this->cart->remove($row_id);
        return redirect()->to(base_url('keranjang/'));
    }

    //method checkout
    public function checkout()
    {
        $cartData = $this->cart->contents();
        $cartDataDecode = json_decode($this->request->getVar('cart_data'), true);
        $totalPrice = 0;
        //generate order code
        $orderCode = $this->order->getMaxCode();
        $urutan = (int) substr($orderCode[0]['max_code'], 4, 4);
        $urutan++;
        $code = "PSN";
        $orderCode = $code . sprintf("%04s", $urutan);

        //insert table order
        $this->order->insert([
			'order_id' => $orderCode,
			'total_price' => $this->request->getVar('total_price'),
			'buyer_name' => $this->request->getVar('buyer_name'),
		]);

        //insert table detail order
        foreach ($cartData as $data) {
            $this->orderDetail->insert([
                'order_id' => $orderCode,
                'book_id' => $data['id'],
                'title' => $data['name'],
                'publisher' => $data['options']['publisher'],
                'total_item' => $data['qty'],
                'price' => $data['price'],
                'subtotal' => $data['subtotal'],
            ]);
        }

        $this->cart->destroy();

        return $this->complete_order($cartDataDecode, $this->request->getVar('buyer_name'), $orderCode);
    }

    public function complete_order($cart, $buyerName, $orderCode) {
        $data["cart"] = $cart;
        $data["order_code"] = $orderCode;
        $data["buyer_name"] = $this->request->getVar('buyer_name');
		session()->setFlashdata('message', 'Pesanan berhasil dibuat!');
        return view('web/checkout', $data);
    }

    //method kosongkan keranjang
    public function clear_cart()
    {
        $this->cart->destroy();
        echo $this->view();
    }

    //respon ke ajax
    public function view()
    {
        $output = '';
        $output .= '
            <h3>Keranjang Belanja</h3> <br/>
            <div class="table-responsive">
            <div align="right">
                <button type="button" id="clear_cart" class="btn btn-sm btn-warning"><i class="fa fa-shopping-cart"></i> Kosongkan</button>
            </div>
                <br/>
                <table class="table table-bordered">
                    <tr>
                        <th width="40%">Name</th>
                        <th width="15%">Jumlah</th>
                        <th width="20%">Harga</th>
                        <th width="20%">Total</th>
                        <th width="5%">#</th>
                    </tr>
        ';
        $count = 0;
        foreach ($this->cart->contents() as $item) {
            $count++;
            $output .= '
            <tr>
                <td>' . $item["name"] . '</td>
                <td>' . $item["qty"] . '</td>
                <td>' . $item["price"] . '</td>
                <td>' . $item["subtotal"] . '</td>
                <td><button type="button" name="remove" class="btn btn-danger btn-sm remove_inventory" id="' . $item['rowid'] . '"><i class="fa fa-trash"></i></button></td>
            </tr>
            ';
        }
        $output .= '
            <tr>
                <td colspan="4" align="right">Total</td>
                <td>' . $this->cart->total() . '</td>
            <tr>
            </table>
            </div>
        ';
        if ($count == 0) {
            $output = "<h3 class='text-center text-danger'><i class='fa fa-shopping-cart'></i> Keranjang belanja kosong</h3>";
        }
        return $output;
    }
}