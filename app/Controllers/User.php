<?php

namespace App\Controllers;

use App\Models\UserModel;

class User extends BaseController
{
	protected $user;

	public function __construct()
	{
		//		parent::__construct();
		$this->user = new UserModel();

		helper('form');
	}

	public function index()
	{
		$data['user'] = $this->user->findAll();
		return view('user/index', $data);
	}

	public function add()
	{
		return view('user/add');
	}

	public function store()
	{
		if (!$this->validate([
			// 'fullname' => [
			// 	'rules' => 'required',
			// 	'errors' => [
			// 		'required' => '{field} Harus diisi'
			// 	]
			// ],
			'username' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Username Harus diisi'
				]
			],
			'password' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Password Harus diisi'
				]
			],

		])) {
			session()->setFlashdata('error', $this->validator->listErrors());
			return redirect()->back()->withInput();
		}

		$this->user->insert([
			'fullname' => $this->request->getVar('fullname'),
			'username' => $this->request->getVar('username'),
			'password' => hash('sha512', ($this->request->getVar('password')))
		]);
		session()->setFlashdata('message', 'Tambah User Berhasil');
		return redirect()->to('/User');
	}

	function edit($id)
	{
		$dataUser = $this->user->find($id);
		if (empty($dataUser)) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data User Tidak ditemukan !');
		}
		$data['user'] = $dataUser;
		return view('user/edit', $data);
	}

	public function update($id)
	{
		if (!$this->validate([
			'fullname' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} Harus diisi'
				]
			],
			'username' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} Harus diisi'
				]
			],
			'password' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} Harus diisi'
				]
			],

		])) {
			session()->setFlashdata('error', $this->validator->listErrors());
			return redirect()->back();
		}

		$this->user->update($id, [
			'fullname' => $this->request->getVar('fullname'),
			'username' => $this->request->getVar('username'),
			'password' => hash('sha512', ($this->request->getVar('password')))
		]);
		session()->setFlashdata('message', 'Update Data User Berhasil');
		return redirect()->to('/User');
	}

	function delete($id)
	{
		$dataUser = $this->user->find($id);
		if (empty($dataUser)) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data User Tidak ditemukan !');
		}
		$this->user->delete($id);
		session()->setFlashdata('message', 'Delete Data User Berhasil');
		return redirect()->to('/User');
	}
}
