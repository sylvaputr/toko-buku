<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>BooKU</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/css/mod.css" rel="stylesheet">
</head>

<body>
    <!-- Topbar Start -->
    <div class="top-bar-booku">
        <div class="container-fluid">
            <div class="row align-items-center bg-light py-3 px-xl-5 d-none d-lg-flex">
                <div class="col-lg-4">
                    <a href="" class="text-decoration-none">
                        <span class="h1 text-uppercase text-white bg-custom px-2">Boo</span>
                        <span class="h1 text-uppercase text-white bg-ku px-2 ml-n1">KU</span>
                    </a>
                </div>
                <div class="col-lg-5 col-6 text-left">
                    <nav class="navbar navbar-expand-lg bg-white navbar-dark py-3 py-lg-0 px-0">
                        <a href="" class="text-decoration-none d-block d-lg-none">
                            <span class="h1 text-uppercase text-dark bg-light px-2">Multi</span>
                            <span class="h1 text-uppercase text-light bg-primary px-2 ml-n1">Shop</span>
                        </a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto py-0">
                                <a href="<?= base_url(); ?>/Web" class=" nav-item nav-link">Beranda</a>
                                <a href="shop.html" class="nav-item nav-link active">Produk</a>
                                <a href="detail.html" class="nav-item nav-link">Blog</a>
                                <a href="contact.html" class="nav-item nav-link">Tentang Kami</a>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="col-lg-3 col-6 text-right">
                    <a href="<?= base_url(); ?>/Keranjang" class="btn px-0 ml-3">
                        <i class="fas fa-shopping-cart"  style="color: #F76E11;"></i>
                        <span class="badge rounded-circle" style="padding-bottom: 2px;"><?= $cart?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->

    <?php if (!empty(session()->getFlashdata('message'))) : ?>
        <div class="alert alert-success alert-dismissible fade show mr-2 ml-2 mt-0" role="alert">
            <?php echo session()->getFlashdata('message'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>

    <!-- Shop Detail Start -->
    <div class="container-fluid mt-4">
        <div class="row px-xl-5">
                <div class="col-lg-2">
                   <img class="img-fluid w-100" src="<?= base_url() . "/uploads/photos/" . $buku['photo']; ?>" alt="">
                </div>

                <div class="col-lg-6 h-auto mb-30 animated-up">
                    <div class="h-100 bg-light p-0">
                        <p class="mb-2"><?= $buku['writer']; ?></p>
                        <h3 class="mb-4"><?= $buku['title']; ?></h3>
                        <strong class="text-dark">Deskripsi Buku</strong>
                        <p class="mb-4 mt-2"><?= $buku['description']; ?> <br>
                            <a href="#">Baca Selengkapnya</a>
                        </p>
                        <strong class="text-dark mr-3">Detail Buku </strong>
                        <div class="row mt-2 detail-buku">
                            <div class="col-lg-6 animated-up">
                                <span>Jumlah Halaman
                                    <p><?= $buku['total_page']; ?></p>
                                </span>

                                <span>Tanggal Terbit
                                    <p><?= $buku['release_date']; ?></p>
                                </span>

                                <span>ISBN
                                    <p><?= $buku['isbn']; ?></p>
                                </span>

                                <span>Bahasa
                                    <p><?= $buku['language']; ?></p>
                                </span>
                            </div>
                            <div class="col-lg-6 animated-up">
                                <span>Penerbit
                                    <p><?= $buku['publisher']; ?></p>
                                </span>

                                <span>Berat
                                    <p><?= $buku['height']; ?> kg</p>
                                </span>

                                <span>Lebar
                                    <p><?= $buku['width']; ?> cm</p>
                                </span>

                                <span>Panjang
                                    <p><?= $buku['height']; ?> cm</p>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-lg-3 h-auto mb-30 animated-up">
                        <div class="card">
                            <div class="card-body pb-0">

                            <form action="<?= base_url(); ?>/Keranjang/add" method="post" class="form">
                                <strong>Ingin beli berapa?</strong>
                                <p class="mt-2" style="font-size: 14px;">Jumlah Barang</p>
                                <div class="stock-input">
                                    <div class="stock-input-left">
                                        <button class="btn-quantity" type="button" onclick="minQty(<?= $buku['price']; ?>)"><i class="fa fa-minus" ></i></button>
                                        <input type="number" id="qty" name="qty" value="1" class="col-lg-4" style="text-align:center;" readonly="true"></input>
                                        <input type="hidden" name="id" value="<?= $buku['book_id']?>"></input>
                                        <input type="hidden" name="price" value="<?= $buku['price']?>"></input>
                                        <input type="hidden" name="name" value="<?= $buku['title']?>"></input>
                                        <input type="hidden" name="publisher" value="<?= $buku['publisher']?>"></input>
                                        <button class="btn-quantity" type="button" onclick="addQty(<?= $buku['price']; ?>)"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <hr>
                                    <div class="subtotal">
                                        <p class="text-sub">Subtotal</p>
                                        <label id="price">Rp<?= number_format($buku['price'], 0, '.', ','); ?></label>
                                    </div>
                                    <button type='submit' class="btn btn-add">Tambahkan ke keranjang</button>
                                </div>
                            
                            </form> 
                            </div>  
                            <div class="row">
                                <div class="col-12">
                                    <div class="chartjs-size-monitor">
                                        <div class="chartjs-size-monitor-expand">
                                            <div class=""></div>
                                        </div>
                                        <div class="chartjs-size-monitor-shrink">
                                            <div class=""></div>
                                        </div>
                                    </div><canvas id="btc-chartjs" class="height-75 chartjs-render-monitor" style="display: block; height: 75px; width: 383px;" width="574" height="112"></canvas>
                                </div>
                            </div>
                        </div>
                    </div> 
        </div>
    </div>
    <!-- Shop Detail End -->


    <!-- Also Like Start -->
    <div class="best-seller-booku">
        <div class="container-fluid pb-3">
            <h2 class="section-title position-relative mx-xl-5 mb-4"><span class="pr-3">Produk Serupa</span></h2>
            <div class="row col-lg-12 px-xl-5">
                <div class="col-lg-3 pb-1 animated-up">
                    <div class="product-item bg-light mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100 height0" src="<?= base_url(); ?>/web-assets/img/4.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Yasunari Kawabata</a>
                            <div class="d-flex mt-2">
                                <p>Ibukota Lama</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 70.000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 pb-1 animated-up">
                    <div class="product-item bg-light mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100 height0" src="<?= base_url(); ?>/web-assets/img/4.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Heru Joni Putra</a>
                            <div class="d-flex mt-2">
                                <p>Suara yang Lebih Keras Catatan dari Makam Tan Malaka</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 80.000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 pb-1 animated-up">
                    <div class="product-item bg-light mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100 height0" src="<?= base_url(); ?>/web-assets/img/1-1-2.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Marco Kartodikromo</a>
                            <div class="d-flex mt-2">
                                <p>Sair Rempah - Rempah</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 99.000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 pb-1 animated-up">
                    <div class="product-item bg-light mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100 height0" src="<?= base_url(); ?>/web-assets/img/1-1-2.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Rene Descartes</a>
                            <div class="d-flex mt-2">
                                <p>Diskursus tentang Metode</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 110.000</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Best Seller End -->


    <!-- Footer Start -->
    <div class="footer container-fluid text-dark">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Tentang BooKU</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Informasi</a>
                            <a class="text-dark mb-2" href="#">Toko Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4 bold">Bantuan</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">FAQ</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Pengembalian</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Privasi</a>
                            <a class="text-dark mb-2" href="#">Aksesibilitas</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Membership</a>
                            <a class="text-dark mb-2" href="#">Profil</a>
                            <a class="text-dark mb-2" href="#">Kupon</a>
                            <a class="text-dark mb-2" href="#">Kontak Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun Sosial BooKU</p>
                        <div class="d-flex btn-footer">
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-square" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border-top py-3" style="background: #F76E11;">
            <div class="col-md-12 px-xl-0">
                <h6 class="mb-md-0 text-center text-white">
                    © 2022. PT Kelompok 1
                </h6>
            </div>
        </div>
    </div>
    <!-- Footer End -->

    <script>
        var qty = 1;
        // window.onload = function(){
        //     // document.getElementById("qty").innerHTML = $qty;
        // };

        function addQty(price) {
            document.getElementById('qty').value = ++qty;
            document.getElementById('price').innerHTML = `Rp${thousands_separators(price * qty)}`;
        }

        function minQty(price) {
           if (qty > 1) {
            document.getElementById('qty').value = --qty;
            document.getElementById('price').innerHTML = `Rp${thousands_separators(price * qty)}`;
           }
            
        }

        function thousands_separators(num)
        {
            var num_parts = num.toString().split(".");
            num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return num_parts.join(".");
        }
    </script>

    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/easing/easing.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?= base_url(); ?>/web-assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?= base_url(); ?>/web-assets/js/main.js"></script>
</body>

</html>