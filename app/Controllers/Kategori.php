<?php

namespace App\Controllers;

use App\Models\KategoriModel;

class Kategori extends BaseController
{

	protected $kategori;

	public function __construct()
	{
		//		parent::__construct();
		$this->kategori = new KategoriModel();

		helper('form');
	}

	public function index()
	{
		$data['kategori'] = $this->kategori->findAll();
		return view('kategori/index', $data);
	}

	public function add()
	{
		return view('kategori/add');
	}

	public function store()
	{
		if (!$this->validate([
			'name' => [
				'rules' => 'required',
				'errors' => [
					'required' => 'Nama Kategori Harus diisi'
				]
			],

		])) {
			session()->setFlashdata('error', $this->validator->listErrors());
			return redirect()->back()->withInput();
		}

		$this->kategori->insert([
			'name' => $this->request->getVar('name')
		]);
		session()->setFlashdata('message', 'Tambah Kategori Berhasil');
		return redirect()->to('/Kategori');
	}

	function edit($id)
	{
		$dataKategori = $this->kategori->find($id);
		if (empty($dataKategori)) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Kategori Tidak ditemukan !');
		}
		$data['kategori'] = $dataKategori;
		return view('kategori/edit', $data);
	}

	public function update($id)
	{
		if (!$this->validate([
			'name' => [
				'rules' => 'required',
				'errors' => [
					'required' => '{field} Harus diisi'
				]
			],

		])) {
			session()->setFlashdata('error', $this->validator->listErrors());
			return redirect()->back();
		}

		$this->kategori->update($id, [
			'name' => $this->request->getVar('name')
		]);
		session()->setFlashdata('message', 'Update Data Kategori Berhasil');
		return redirect()->to('/Kategori');
	}

	function delete($id)
	{
		$dataKategori = $this->kategori->find($id);
		if (empty($dataKategori)) {
			throw new \CodeIgniter\Exceptions\PageNotFoundException('Data Kategori Tidak ditemukan !');
		}
		$this->kategori->delete($id);
		session()->setFlashdata('message', 'Delete Data Kategori Berhasil');
		return redirect()->to('/Kategori');
	}
}
