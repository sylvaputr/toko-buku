<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>BooKU</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url(); ?>/web-assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url(); ?>/web-assets/css/mod.css" rel="stylesheet">
</head>

<body>
    <!-- Topbar Start -->
    <div class="top-bar-booku">
        <div class="container-fluid">
            <div class="row align-items-center bg-light py-3 px-xl-5 d-none d-lg-flex">
                <div class="col-lg-4">
                    <a href="" class="text-decoration-none">
                        <span class="h1 text-uppercase text-white bg-custom px-2">Boo</span>
                        <span class="h1 text-uppercase text-white bg-ku px-2 ml-n1">KU</span>
                    </a>
                </div>
                <div class="col-lg-5 col-6 text-left">
                    <nav class="navbar navbar-expand-lg bg-white navbar-dark py-3 py-lg-0 px-0">
                        <a href="" class="text-decoration-none d-block d-lg-none">
                            <span class="h1 text-uppercase text-dark bg-light px-2">Multi</span>
                            <span class="h1 text-uppercase text-light bg-primary px-2 ml-n1">Shop</span>
                        </a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto py-0">
                                <a href="<?= base_url(); ?>/Web" class=" nav-item nav-link active">Beranda</a>
                                <a href="shop.html" class="nav-item nav-link">Produk</a>
                                <a href="detail.html" class="nav-item nav-link">Blog</a>
                                <a href="contact.html" class="nav-item nav-link">Tentang Kami</a>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="col-lg-3 col-6 text-right">
                    <a href="<?= base_url(); ?>/Keranjang" class="btn px-0 ml-3">
                        <i class="fas fa-shopping-cart" style="color: #F76E11;"></i>
                        <span class="badge rounded-circle" style="padding-bottom: 2px;"><?= $cart?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->
    <!-- Carousel Start -->
    <div class="carousel-booku">
        <div class="container-fluid">
            <div class="row px-xl-5">
                <div class="col-lg-8 animated fadeInLeft">
                    <div id="header-carousel" class="carousel slide carousel-fade mb-30 mb-lg-0" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#header-carousel" data-slide-to="1"></li>
                            <li data-target="#header-carousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item position-relative active" style="height: 430px;">
                                <img class="position-absolute w-100 h-100" src="<?= base_url(); ?>/web-assets/img/banner/4.jpg" style="object-fit: cover;">
                                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                    <div class="p-3" style="max-width: 700px;">
                                        <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">BookU</h1>
                                        <p class="mx-md-5 px-5 animate__animated animate__bounceIn">Lorem rebum magna
                                            amet
                                            lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr ipsum diam
                                        </p>
                                        <form action="">
                                            <div class="input-group">
                                                <input type="text" class="form-control search" placeholder="Cari judul buku atau penulis">
                                                <div class="input-group-append">
                                                    <span class="input-group-text search-icon bg-transparent text-primary">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item position-relative" style="height: 430px;">
                                <img class="position-absolute w-100 h-100" src="<?= base_url(); ?>/web-assets/img/banner/5.jpg" style="object-fit: cover;">
                                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                    <div class="p-3" style="max-width: 700px;">
                                        <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">
                                            Women
                                            Fashion</h1>
                                        <p class="mx-md-5 px-5 animate__animated animate__bounceIn">Lorem rebum magna
                                            amet
                                            lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr ipsum diam
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item position-relative" style="height: 430px;">
                                <img class="position-absolute w-100 h-100" src="<?= base_url(); ?>/web-assets/img/banner/6.jpg" style="object-fit: cover;">
                                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                    <div class="p-3" style="max-width: 700px;">
                                        <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">Kids
                                            Fashion</h1>
                                        <p class="mx-md-5 px-5 animate__animated animate__bounceIn">Lorem rebum magna
                                            amet
                                            lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr ipsum diam
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 animated fadeInRight">
                    <div class="product-offer mb-30" style="height: 200px;">
                        <img class="img-fluid" src="<?= base_url(); ?>/web-assets/img/banner/5.jpg" alt="">
                        <div class="offer-text">
                            <h6 class="text-white text-uppercase">Save 20%</h6>
                            <h3 class="text-white mb-3">Special Offer</h3>
                        </div>
                    </div>
                    <div class="product-offer mb-30" style="height: 200px;">
                        <img class="img-fluid" src="<?= base_url(); ?>/web-assets/img/banner/6.jpg" alt="">
                        <div class="offer-text">
                            <h6 class="text-white text-uppercase">Save 20%</h6>
                            <h3 class="text-white mb-3">Special Offer</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Carousel End -->

    <!-- Best Seller Start -->
    <div class="best-seller-booku">
        <div class="container-fluid pt-5 pb-3">
            <h2 class="section-title position-relative mx-xl-5 mb-4"><span class="pr-3">Best
                    Seller</span></h2>
            <div class="row col-lg-12 px-xl-5">
                <?php
                foreach ($buku as $row) {
                ?>
                    <div class="col-lg-2 pb-1 animated-up">
                        <div class="product-item bg-light mb-4">
                            <div class="product-img position-relative overflow-hidden">
                                <img class="img-fluid w-100" src="<?= base_url() . "/uploads/photos/" . $row['photo']; ?>" alt="">
                                <div class="product-action">
                                    <a class="btn btn-outline-dark btn-square" href="<?= base_url(); ?>/Web/Detail/<?= $row['book_id']; ?>"><i class=" fa fa-search"></i></a>
                                </div>
                            </div>
                            <div class="p-2">
                                <a class="h6 text-decoration-none text-truncate"><?= $row['writer']; ?></a>
                                <a class="h4 text-truncate" href="<?= base_url(); ?>/Web/Detail/<?= $row['book_id']; ?>"><h6><?= $row['title']; ?></h6></a>
                                <div class="price d-flex">
                                    <p>Rp<?= number_format($row['price'], 0, '.', ','); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
    <!-- Best Seller End -->

    <!-- New Arrivals Start -->
    <div class="new-arrivals-booku" style="background: url('<?= base_url(); ?>/web-assets/img/booku.png');">
        <div class="container-fluid pt-5">
            <h2 class="section-title position-relative">New Arrivals</h2>
            <div class="row col-lg-12 px-xl-5 justify-content-center">
                <div class="col-lg-2 pb-1 animated-up">
                    <div class="product-item mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="<?= base_url(); ?>/web-assets/img/1-1-2.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Yasunari Kawabata</a>
                            <div class="d-flex mt-2">
                                <p class="text-white">Ibukota Lama</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 70.000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 pb-1 animated-up">
                    <div class="product-item mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="<?= base_url(); ?>/web-assets/img/2.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Heru Joni Putra</a>
                            <div class="d-flex mt-2">
                                <p class="text-white">Suara yang Lebih Keras Catatan dari Makam Tan Malaka</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 80.000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 pb-1 animated-up">
                    <div class="product-item mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="<?= base_url(); ?>/web-assets/img/4.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Marco Kartodikromo</a>
                            <div class="d-flex mt-2">
                                <p class="text-white">Sair Rempah - Rempah</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 99.000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 pb-1 animated-up">
                    <div class="product-item mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="<?= base_url(); ?>/web-assets/img/15-1.png" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="p-2">
                            <a class="h6 text-decoration-none text-truncate" href="">Eko Darmoko</a>
                            <div class="d-flex mt-2">
                                <p class="text-white">Revolusi Nuklir</p>
                            </div>
                            <div class="price d-flex">
                                <p>Rp. 85.000</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- New Arrivals End -->

    <!-- Trending Start -->
    <div class="blog-booku">
        <div class="container-fluid pt-5 pb-3">
            <h2 class="section-title position-relative mx-xl-5 mb-4"><span class="pr-3">Blog dari BooKU</span></h2>
            <div class="row">
                <div class="col-lg-6 mb-4 ml-xl-5">
                    <img class="w-100 rounded animated-up img-one mb-2" src="<?= base_url(); ?>/uploads/photos/2.png" alt="">
                    <p>23 Januari 2022</p>
                    <h1>Manfaat Mendongeng untuk Anak</h1>
                    <h5>Ketika kita masih kecil, kita pasti pernah di bacakan dongen oleh orang tua kita, dan mungkin waktu di bacakan dongen itu adalah waktu yang selalu kita nantikan.</h5>
                    <a href="#">Baca Selengkapnya</a>
                </div>
                <div class="col-lg-5 pl-xl-4">
                    <div class="row">
                        <div class="col-lg-3 p-0 animated-up">
                            <img class="img-fluid rounded" src="<?= base_url(); ?>/web-assets/img/banner/baca.jpg" alt="">
                        </div>
                        <div class="col-lg-8 animated-up p-0 come-in ml-3">
                            <p>23 Januari 2022</p>
                            <h1>Membaca Buku untuk Hidup Lebih Lama</h1>
                            <h5>Sempatkanlah membaca di waktu luang, jika ingin hidup lebih lama. Studi terbaru dari Yale University’s School Public Helth mengungkap bahwa orang yang banyak membaca hidupnya bisa dua tahun lebih lama.</h5>
                            <a href="#">Baca Selengkapnya</a>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-3 p-0 animated-up">
                            <img class="img-fluid rounded animated-up" src="<?= base_url(); ?>/web-assets/img/banner/paket.jpg" alt="">
                        </div>
                        <div class="col-lg-8 animated-up p-0 come-in ml-3">
                            <p>23 Januari 2022</p>
                            <h1>BooKU Melayani Pengiriman ke Seluruh Penjuru Negeri</h1>
                            <h5>Tahun 2022 ini kami bertekad untuk menjangkau lebih luas strategi pemasaran, hal ini disampaikan oleh John Doe selaku Direktur dari Booku.</h5>
                            <a href="#">Baca Selengkapnya</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Trending End -->

    <!-- Footer Start -->
    <div class="footer container-fluid text-dark">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Tentang BooKU</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Informasi</a>
                            <a class="text-dark mb-2" href="#">Toko Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4 bold">Bantuan</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">FAQ</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Pengembalian</a>
                            <a class="text-dark mb-2" href="#">Kebijakan Privasi</a>
                            <a class="text-dark mb-2" href="#">Aksesibilitas</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun</p>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="#">Membership</a>
                            <a class="text-dark mb-2" href="#">Profil</a>
                            <a class="text-dark mb-2" href="#">Kupon</a>
                            <a class="text-dark mb-2" href="#">Kontak Kami</a>
                        </div>
                    </div>
                    <div class="col-md-3 mb-5">
                        <p class="text-dark text-uppercase mb-4">Akun Sosial BooKU</p>
                        <div class="d-flex btn-footer">
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-square mr-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-square" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border-top py-3" style="background: #F76E11;">
            <div class="col-md-12 px-xl-0">
                <h6 class="mb-md-0 text-center text-white">
                    © 2022. PT Kelompok 1
                </h6>
            </div>
        </div>
    </div>
    <!-- Footer End -->

    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/wow/wow.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/easing/easing.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/waypoints/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/counterup/counterup.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/lib/lightbox/js/lightbox.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?= base_url(); ?>/web-assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?= base_url(); ?>/web-assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?= base_url(); ?>/web-assets/js/main.js"></script>
</body>

</html>