<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{
    protected $table      = 'order';
    protected $primaryKey = 'order_id';

    protected $useAutoIncrement = true;
    protected $allowedFields = ['order_id', 'total_price', 'created_at', 'status', 'buyer_name'];

    public function getMaxCode()
    {
        $builder = $this->db->table('order')->selectMax('order_id', 'max_code');
        return $builder->get()->getResultArray();  
    }

    public function getReportData ()
    {
         return $this->db->table('order')
         ->join('detail_order','detail_order.order_id=order.order_id')->where('status', 1)
         ->get()->getResultArray();  
    }

    public function sumQuantities ()
    {
         return $this->db->table('order')->join('detail_order','detail_order.order_id=order.order_id')->where('status', 1)->selectSum('total_item')->get()->getResultArray();  
    }

    public function sumRevenue ()
    {
         return $this->db->table('order')->selectSum('total_price')->where('status', 1)->get()->getResultArray();  
    }
}
