<?php

namespace App\Controllers;
use App\Models\OrderModel;
use App\Models\OrderDetailModel;

class Site extends BaseController
{
	public function __construct()
	{
		//		parent::__construct();
		$this->db = \Config\Database::connect();
		$this->email = \Config\Services::email();
		$this->order = new OrderModel();
        $this->orderDetail = new OrderDetailModel();

		helper('form');
	}

	public function index()
	{
		$qty= $this->order->sumQuantities();
        $revenue = $this->order->sumRevenue();
		$data['totalQty'] = $qty[0]['total_item'];
		$data['totalRevenue'] = $revenue[0]['total_price'];
		return view('menu/dashboard', $data);
	}

	public function login()
	{
		return view('site/login');
	}

	public function dashboard()
	{
		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');
		$hashPassword = hash('sha512', $password);
		$respon = $this->getUserName($username, $hashPassword);

		if ($respon != '') {
			$session = session();
			$session->set('namaUser', $respon);
			//dashboard data
			$qty= $this->order->sumQuantities();
        	$revenue = $this->order->sumRevenue();
			$data['totalQty'] = $qty[0]['total_item'];
			$data['totalRevenue'] = $revenue[0]['total_price'];
			return view('menu/dashboard', $data);
		} else {
			echo "<script type='text/javascript'>alert('Username atau Password Salah !');</script>";
			return view('site/login');
		}
	}

	public function getUserName($username, $password)
	{
		$query = $this->db->query("select fullname from user where username = '$username' and password = '$password'");
		$row   = $query->getRow();
		return isset($row->fullname) ? $row->fullname : '';
	}

	public function logout()
	{
		session()->destroy();
		return $this->login();
	}
}
