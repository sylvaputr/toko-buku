<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include_once('_meta.php');
    ?>

    <?php
    include_once('_css.php');
    ?>

</head>

<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <!-- Navbar -->
    <?php
    include_once('_navbar.php');
    ?>
    <!-- /.navbar -->

    <?php
    include_once('_sidebar.php');
    ?>

    <?php
    include_once('_content.php');
    ?>

    <?php
    include_once('_footer.php');
    ?>

    <?php
    include_once('_js.php');
    ?>

</body>

</html>