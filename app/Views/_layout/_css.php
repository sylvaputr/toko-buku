<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/vendors/css/weather-icons/climacons.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/fonts/meteocons/style.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/vendors/css/charts/morris.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/vendors/css/charts/chartist.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/bootstrap-extended.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/colors.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/components.css">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/core/menu/menu-types/vertical-menu-modern.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/fonts/simple-line-icons/style.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/pages/timeline.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/app-assets/css/pages/dashboard-ecommerce.css">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/css/style.css">
<!-- END: Custom CSS-->