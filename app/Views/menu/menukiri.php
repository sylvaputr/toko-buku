<!-- BEGIN: Main Menu-->

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class="active"><a href="<?= base_url(); ?>/Site"><i class="la la-home"></i><span class="menu-title" data-i18n="eCommerce Dashboard">Dashboard</span></a>
      </li>
      <li class=" navigation-header"><span data-i18n="Ecommerce">Data</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Ecommerce"></i>
      </li>
      <li class=" nav-item"><a href="<?= base_url(); ?>/Kategori"><i class="la la-th-large"></i><span class="menu-title" data-i18n="Shop">Kategori</span></a>
      </li>
      <li class=" nav-item"><a href="<?= base_url(); ?>/Buku"><i class="la la-list"></i><span class="menu-title" data-i18n="Product Detail">Buku</span></a>
      </li>
      <li class=" nav-item"><a href="<?= base_url(); ?>/User"><i class="la la-user"></i><span class="menu-title" data-i18n="Shopping Cart">User</span></a>
      </li>
      <li class=" navigation-header"><span data-i18n="Ecommerce">Pesanan</span><i class="la la-ellipsis-h" data-toggle="tooltip" data-placement="right" data-original-title="Ecommerce"></i>
      </li>
      <li class="nav-item"><a href="<?= base_url(); ?>/Order"><i class="la la-th-large"></i><span class="menu-title" data-i18n="Shop">Daftar Pesanan</span></a>
      </li>
      <li class=" nav-item"><a href="<?= base_url(); ?>/Order/Report"><i class="la la-list"></i><span class="menu-title" data-i18n="Product Detail">Laporan Penjualan</span></a>
      </li>
    </ul>
  </div>
</div>

<!-- END: Main Menu-->